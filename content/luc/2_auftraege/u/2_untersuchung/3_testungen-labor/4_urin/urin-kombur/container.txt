Title: Kombur-Testung

----

Text: 

----

Fiitems: 

- 
  label: Leukozyten
  help: ""
  value: negativ
  sourceid: FI_LEUKOZY
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: Nitrit
  help: ""
  value: negativ
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: pH
  help: ""
  value: "5"
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: Eiweiss
  help: ""
  value: negativ
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: Glucose
  help: ""
  value: negativ
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: Keton
  help: ""
  value: negativ
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""
- 
  label: Blut im Harn
  help: ""
  value: negativ
  sourceid: ""
  fitype: fitext
  ficustom: ""
  hint: ""