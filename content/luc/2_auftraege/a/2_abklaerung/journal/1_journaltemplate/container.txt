Title: neuer Journaleintrag

----

Text: 

----

Novalues: 0

----

Sortable: 0

----

Fiitems: 

- 
  label: Titel
  hint: ""
  help: ""
  value: ""
  placeholder: >
    Titel kann auch automatisch übernommen
    werden
  sourceid: J_TITEL
  fitype: fitext
  ficustom: ""
  reqfield: true
  selectvalues: ""
- 
  label: Leistungs-Datum
  hint: ""
  help: ""
  value: 2019-01-17
  placeholder: ""
  sourceid: J_DATE
  fitype: fidatum
  ficustom: ""
  reqfield: true
  selectvalues: ""
  span: 6
- 
  label: Leistungserbringung durch
  hint: ""
  help: ""
  value: Marc Geiser
  placeholder: ""
  sourceid: J_EMPLOYEE
  fitype: fimitarbeiter
  ficustom: ""
  reqfield: true
  selectvalues: ""
- 
  label: Tätigkeit
  hint: ""
  help: ""
  value: ""
  placeholder: ""
  sourceid: J_ACTIVITY
  fitype: firadio
  ficustom: ""
  reqfield: true
  selectvalues: >
    Aktenstudium,Korrespondenz,persönlicher
    Kontakt,Qualitätssicherung,Diagnose
    hinzufügen,Anderes
  span: 3
- 
  label: Bemerkung
  hint: ""
  help: ""
  value: ""
  placeholder: >
    Hier können Sie einen zum Vorgang
    hinterlegen
  sourceid: J_COMMENT
  fitype: fitextarea
  ficustom: ""
  reqfield: false
  selectvalues: ""
- 
  label: Dauer
  hint: ""
  help: ""
  value: ""
  placeholder: ""
  sourceid: J_DURATION
  fitype: fidauer
  ficustom: ""
  reqfield: true
  selectvalues: ""

----

Sourceid: C_JOURNAL

----

Istemplate: 1