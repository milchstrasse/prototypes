Title: sonstiger Hörverlust

----

Fiitems: 

- 
  label: Festgestellt am
  value: Oktober 2015
  value2: ""
  hint: ""
  help: ""
  sourceid: DIAGVOM
  fireadonly: 'false'
  hidden: 'false'
  fitype: fitext
  selectvalues: ""
  ficustom: ""
  span: ""
- 
  label: Diagnose
  value: H91
  value2: sonstiger Hörverlust
  hint: ""
  help: ""
  sourceid: DIAGTEXT
  fireadonly: 'false'
  hidden: 'false'
  fitype: ficustom
  selectvalues: ""
  ficustom: cus-icd
  span: "4"
- 
  label: Kommentar
  value: C3/4 Senke
  value2: ""
  hint: ""
  help: ""
  sourceid: ""
  fireadonly: 'false'
  hidden: 'false'
  fitype: fitextarea
  selectvalues: ""
  ficustom: ""
  span: ""
- 
  label: Hinzugefügt von
  value: Rosemarie Albisser
  value2: ""
  hint: ""
  help: ""
  sourceid: ""
  fireadonly: 'false'
  hidden: 'false'
  fitype: ficustom
  selectvalues: ""
  ficustom: cus-aerzteselect
  span: ""
- 
  label: Hinzugefügt am
  value: 2010-02-03
  value2: ""
  hint: ""
  help: ""
  sourceid: ""
  fireadonly: 'false'
  hidden: 'false'
  fitype: fidatum
  selectvalues: ""
  ficustom: ""
  span: ""
- 
  label: aktiv in diesem Auftrag
  value: ja
  value2: ""
  hint: ""
  help: ""
  sourceid: ""
  fireadonly: 'false'
  hidden: 'false'
  fitype: firadio
  selectvalues: ja,nein
  ficustom: ""
  span: ""

----

Text: 

----

Novalues: false

----

Sortable: false

----

Istemplate: false

----

Sourceid: DIAGNOSE

----

Customsnippet: 

----

Params: 