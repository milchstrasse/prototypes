class App {
  static main() {
    console.log("App2 started!");
    $("body").removeClass("preload");
    App.setupEvents();
  }

  static setupEvents() {
    $('.accordion-header').click(event => {
      console.log("icon geklickt!");

      let target = $(event.currentTarget);

      target.closest('.accordion').toggleClass("closed");

    });
  }
}

$(document).ready(() => App.main());
