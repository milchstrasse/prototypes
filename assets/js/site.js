const DATA_MANUALLY_EDITED = "manually-edited";
const EDIT_CLASS = "edit-modus";
const READ_CLASS = "read-modus";
const OPEN_CLASS = "open";
const HIDDEN_CLASS = "hidden";
const SELECTED_CLASS = "selected";
const DEBUG_CLASS = "debug";
const PRINT_ICON = "fa-print";
const PRINT_CLASS = "print";
const DONTPRINT_ICON = "fa-print-slash";
const DONTPRINT_CLASS = "dontprint";

//$(document.body).on("click", ".radio", () => {})
// event.stopPropagation();

class App {
  static main() {
    console.log("App started!");
    $("body").removeClass("preload");

    App.setupFormEvents();

    App.setupActionBarEvents();
    App.setupContainerHandling();
    App.setupDiagnoseEvents();
    App.setupEntscheidEvents();

    App.setupHeaderEvents();
    App.setupJournalEvents();
    App.setupModalEvents();
    App.setupEvents();

    // Initialisieren
    Containers.initIcons();
    Actions.numberAllContainers();

  }

  static setupActionBarEvents() {
    $('.actionbar .modus').on('click', event => {

      let modusIcon = $(event.currentTarget);
      let wasEDIT = modusIcon.hasClass(EDIT_CLASS);
      let accordions = $('.accordion');

      if (wasEDIT) {
        Containers.toRead(accordions);
        modusIcon.removeClass(EDIT_CLASS);
        modusIcon.addClass(READ_CLASS);
      } else {
        Containers.toEdit(accordions);
        modusIcon.removeClass(READ_CLASS);
        modusIcon.addClass(EDIT_CLASS);
        //        Containers.openAllAccordions('.content');
      }
    });

    $('.actionbar .save').on('click', event => {
      $(event.currentTarget).removeClass("tosave");

      $('.accordion-header .save').addClass(HIDDEN_CLASS);
    });

    $('.actionbar .help').click(event => {
      $(event.currentTarget).toggleClass("help-on");

      $('.infos .help').toggleClass(HIDDEN_CLASS);
    });

    $(document.body).on("click", ".actionbar .journal", () => {
      let createdJournal = Actions.addNewJournal();
      Containers.toEdit(createdJournal);
      Actions.timeMeasure(createdJournal);
    });

    $(document.body).on("click", ".actionbar .print", event => {

      $('#printdialog').removeClass(HIDDEN_CLASS);

      event.stopPropagation();
      event.preventDefault();

    });

    //    FormElements.setReadValueRadioItem();
    //    FormElements.setReadValueTextfield();
  }

  static setupContainerHandling() {
    $(document.body).on("click", ".display .compress", event => {
      Containers.closeAllAccordions($('.content'));
      event.stopPropagation();

    });

    $(document.body).on("click", ".display .expand", event => {
      Containers.openAllAccordions($('.content'));
      event.stopPropagation();
    });

    $(document.body).on("click", ".actions .compress", event => {

      let closestAccordionBody = $(event.currentTarget)
        .closest('.accordion')
        .children('.accordion-body');
      Containers.closeAllAccordions(closestAccordionBody);

      event.stopPropagation();
    });

    $(document.body).on("click", ".actions .expand", event => {

      let closestAccordion = $(event.currentTarget).closest('.accordion');
      let closestAccordionBody = closestAccordion.children('.accordion-body');

      Containers.openAccordion(closestAccordion);
      Containers.openAllAccordions(closestAccordionBody);

      event.stopPropagation();
    });

    $(document.body).on("click", ".actions li.print", event => {

      Actions.togglePrintMarker($(event.currentTarget));
      Actions.numberAllContainers();

      event.stopPropagation();

    });

    $(document.body).on("click", ".actions li.save", event => {

      Actions.saveButtonClicked($(event.currentTarget));
      Containers.toRead($(event.currentTarget));
      event.stopPropagation();
    });

    $(document.body).on("click", ".accordion-header", event => {
      let accordion = $(event.currentTarget).closest('.accordion');
      Containers.toggleAccordion(accordion);
      event.stopPropagation();
    });

    $(document.body).on("click", ".accordion-header .modus", event => {
      Containers.toggleEditModus($(event.currentTarget));
      event.stopPropagation();
    });
  }

  static setupDiagnoseEvents() {

    $(".p-diagnosen .container2").each(function (index, element) {

      let diagnoses = $(element);
      let diagnosesText = diagnoses.find(".feedbackitem.DIAGTEXT .read .pos2").text();
      let diagnosesDate = diagnoses.find(".feedbackitem.DIAGVOM .read .pos1").text();

      diagnoses
        .find(".accordion-header .pos3")
        .html(diagnosesDate);
      diagnoses
        .find(".accordion-header .pos1")
        .addClass("ellipsis");
    });

    $(document.body).on("change", ".DIAGVOM input[type=text]", event => {

      let target = $(event.currentTarget);
      var newText = target.val();

      if (!newText) newText = '';
      newText = newText.trim();

      target
        .closest(".accordion")
        .find(".accordion-header .pos3")
        .text(newText);

    });

    $(document.body).on("click", ".DIAGTEXT .radioitem", event => {

      let target = $(event.currentTarget);
      var newText = target.text();
      var newICD = target.data("value");


      if (!newText) newText = '';
      newText = newText.trim();

      target
        .closest(".accordion")
        .find(".accordion-header .pos1")
        .text(newText);

      target
        .closest(".accordion-body")
        .find(".icd10suche input")
        .val(newICD);

      target
        .closest(".accordion-body")
        .find(".texticd10")
        .text(newText);

    });
  }

  static setupEntscheidEvents() {

    $(document.body).on("click", ".E_ENTSCHEID .radioitem", event => {
      Actions.entscheidClicked($(event.currentTarget));
    });
  }

  static setupEvents() {
    // Versionsauswahl geklickt

    // Button für Modal geklickt (Leistungsrenderer)
    $(document.body).on("click", ".feedbackitem .modalopen", event => {
      let feedbackitem = $(event.currentTarget).closest('.feedbackitem');

      feedbackitem
        .find('.modal.main')
        .removeClass('hidden');
    });

    // Modal bestätigt
    $(document.body).on("click", ".feedbackitem .modal.main .confirm", event => {
      let feedbackitem = $(event.currentTarget).closest('.feedbackitem');

      feedbackitem
        .find('.modal')
        .addClass('hidden');

      feedbackitem
        .find('.feedback')
        .removeClass('hidden');

      feedbackitem
        .find('.edit')
        .addClass('hidden');

      feedbackitem
        .find('.original')
        .addClass('hidden');

    });

    // Löschicon geklickt
    $(document.body).on("click", ".feedbackitem .icon.delete", event => {
      let feedbackitem = $(event.currentTarget).closest('.feedbackitem');

      feedbackitem
        .find('.delete')
        .removeClass('hidden');
    });

    // Löschen bestätigt
    $(document.body).on("click", ".feedbackitem .modal.delete .confirm", event => {
      let feedbackitem = $(event.currentTarget).closest('.feedbackitem');
      //      debugger;
      feedbackitem
        .find('.small')
        .addClass('hidden');

      feedbackitem
        .find('.feedback')
        .addClass('hidden');

      feedbackitem
        .find('.edit')
        .removeClass('hidden');

    });


    $('.menu-toggle').click(event => {
      let mainnavClass = $(event.currentTarget).closest('.mainnav');

      mainnavClass.toggleClass('display-small');
      mainnavClass.toggleClass('display-full');
    });

    $('.mainnav .hasChildren').click(event => {
      let toggleSubmenu = $(event.currentTarget);

      toggleSubmenu.toggleClass("closed");
      toggleSubmenu.toggleClass("open");
    });

    $('.checkbox').click(event => {

      console.log("Checkbox .checkbox clicked!");

      const ICON_CHECKED = "fa-check";
      const ICON_UNCHECKED = "fa-square";

      let checkbox = $(event.currentTarget);
      let checkboxInput = checkbox.find('input');
      let checkboxIcon = checkbox.find('.checkmark');


      checkboxIcon.removeClass(ICON_UNCHECKED);
      checkboxIcon.removeClass(ICON_CHECKED);

      if (checkboxInput.prop("checked")) {
        console.log("Checkbox checked!");
        checkboxIcon.addClass(ICON_CHECKED);
      } else {
        console.log("Checkbox not checked!");
        checkboxIcon.addClass(ICON_UNCHECKED);
      }

    });

    $('.check_von_bis input[type="checkbox"]').click(event => {

      console.log("Checkbox von bis clicked!");

      const ICON_CHECKED = "fa-check";
      const ICON_UNCHECKED = "fa-square";
      const DATE_VISIBLE = "visible";
      const DATE_HIDDEN = "hidden";

      let checkbox = $(event.currentTarget);
      let checkboxIcon = checkbox.siblings('.checkmark');
      let dates = checkbox.closest('.value').find('.date');


      checkboxIcon.removeClass(ICON_UNCHECKED);
      checkboxIcon.removeClass(ICON_CHECKED);
      dates.removeClass(DATE_HIDDEN);
      dates.removeClass(DATE_VISIBLE);

      if (checkbox.prop("checked")) {
        console.log("Checkbox checked!");
        checkboxIcon.addClass(ICON_CHECKED);
        dates.addClass(DATE_VISIBLE);
      } else {
        console.log("Checkbox not checked!");
        checkboxIcon.addClass(ICON_UNCHECKED);
        dates.addClass(DATE_HIDDEN);
      }

    });
    $('.custom_vminfo [type="checkbox"]').click(event => {

      console.log("vminfo clicked!");

      const ICON_CHECKED = "fa-check";
      const ICON_UNCHECKED = "fa-square";
      const DATE_VISIBLE = "visible";
      const DATE_HIDDEN = "hidden";

      let checkbox = $(event.currentTarget);
      let checkboxIcon = checkbox.siblings('.checkmark');
      let dates = checkbox.closest('.value').find('.multirows');

      checkboxIcon.removeClass(ICON_UNCHECKED);
      checkboxIcon.removeClass(ICON_CHECKED);
      dates.removeClass(DATE_HIDDEN);
      dates.removeClass(DATE_VISIBLE);

      if (checkbox.prop("checked")) {
        console.log("Checkbox checked!");
        checkboxIcon.addClass(ICON_CHECKED);
        dates.addClass(DATE_VISIBLE);
      } else {
        console.log("Checkbox not checked!");
        checkboxIcon.addClass(ICON_UNCHECKED);
        dates.addClass(DATE_HIDDEN);
      }
    });

    $('div[contenteditable]').click(event => {

      let target = $(event.currentTarget);

      Actions.activateSaveIcon(target);

    })

    // Toast einblenden, wenn erfolgreich gespeichert wurde
    $('i.save').on('click', event => {
      let toast = $('.toast.success');
      toast.removeClass("hidden");

      window.setTimeout(() => {
        toast.addClass("fade-out");

        window.setTimeout(() => {
          toast
            .removeClass("fade-out")
            .addClass("hidden");
        }, 3100);
      }, 3000);
    });


    // Sortieren per drag'n'drop --> jQueryUI
    $(function () {
      $("#EL_AUFB .tablevalues").sortable();
      $("#EL_AUFB .tablevalues").disableSelection();

      $(".C_DIAGNOSEN .accordion-body").sortable({
        placeholder: "diagnose-ui-state-highlight",
        containment: ".C_DIAGNOSEN",
        scroll: false,
        handle: ".pospilot",
        opacity: 0.7,
        helper: "clone",
        stop: function () {
          Actions.numberAllContainers();
        }
      }).disableSelection();

    });
  }

  static setupFormEvents() {

    $(document.body).on("click", ".radioitem", event => {

      FormElements.radioElementClicked($(event.currentTarget));

      Actions.activateSaveIcon($(event.currentTarget));
    });

    $(document.body).on("input", "input", event => {

      Actions.activateSaveIcon($(event.currentTarget));
    });


  };

  static setupHeaderEvents() {

    $(document.body).on("click", "#benutzer", event => {
      // Benutzerauswahl geklickt (Auswahlliste Hiddenlist)
      GuiElements.showList($(event.currentTarget));
    });

    $(document.body).on("click", "#benutzer li", event => {
      // Liste Benutzer geklickt
      let element = $(event.currentTarget);
      let targetLabel = $('#benutzer p');
      GuiElements.changeListValue(element, targetLabel);

      let userRole = element.data("role");

      $(document.body)
        .toggleClass("role-arzt", userRole == "Arzt")
        .toggleClass("role-qm-arzt", userRole == "QM-Arzt")
        .toggleClass("role-casemanagement", userRole == "CaseManager")
        .toggleClass("role-backoffice", userRole == "BackOffice");
    });

    // Versionsauswahl geklickt (Auswahlliste Hiddenlist)
    $(document.body).on("click", "#version", event => {
      GuiElements.showList($(event.currentTarget));
    });

    // Liste Version geklickt
    $(document.body).on("click", "#version li", event => {

      GuiElements.changeListValue($(event.currentTarget), $('#version p'))

      let version = $(event.currentTarget).data("version");

      $(document.body)
        .toggleClass("standard", version == "standard")
        .toggleClass("variante1", version == "variante1")
        .toggleClass("variante2", version == "variante2")
    });


    $(document.body).on("click", ".main-subnav a", event => {
      console.log("Subnavi geklickt");

      event.preventDefault();

      let targetSelector = event.currentTarget.hash;
      let targetToScrollTo = $(targetSelector);

      Navigation.scrollToTarget(targetToScrollTo);
    });


  };

  static setupJournalEvents() {

    $('.container2.C_JOURNAL').each(function (index, element) {
      console.log(" Setze JournalTitel");
      var edit = $(element)
      //      edit.addClass(DEBUG_CLASS);
      Containers.setJournalTitle(edit);
    });

    $(document.body).on("change", "input#J_TITEL ", event => {

      let target = $(event.currentTarget);
      let accordion = target.closest('.accordion');

      target.data(DATA_MANUALLY_EDITED, true);

      Containers.setJournalTitle(accordion);
    });

    $(document.body).on("change", "input#J_DATE ", event => {

      let target = $(event.currentTarget);
      let accordion = target.closest('.accordion');

      Containers.setJournalTitle(accordion);
    });

    $(document.body).on("click", ".J_ACTIVITY .radioitem", event => {

      let target = $(event.currentTarget);
      let accordion = target.closest('.accordion');

      FormElements.setJournalFieldTitle(target);
      Containers.setJournalTitle(accordion);

    });

    $(document.body).on("click", ".J_DURATION input", event => {

      let target = $(event.currentTarget);

      Actions.timeMesureStopp(target);

    });

    // Journal: Zeitmessung stoppen, wenn in die Box geklickt wird
    $('.J_DURATION input').focus(event => {
      //      Actions.timeMesureStopp($(event.currentTarget));
    })
  }

  static setupModalEvents() {
    $(document.body).on("click", "#printdialog .all ", event => {

      Containers.toRead($('.accordion'));

      Containers.openAllAccordions($('.content'));

      Containers.toPrint($('.accordion'));

      Actions.numberAllContainers();

      $('#printdialog').addClass(HIDDEN_CLASS);


    });

    $(document.body).on("click", "#printdialog .untersuch ", event => {

      Containers.toRead($('.accordion'));

      Containers.openAllAccordions($('.content'));

      Containers.toPrint($('#vitaldaten'));
      Containers.toPrint($('#testungen-labor'));
      Containers.toPrint($('#testungen-labor'));
      Containers.toPrint($('#klinischer-teil'));

      Containers.toNotPrint($('#zusatzinformationen'));
      Containers.toNotPrint($('#anamnese'));

      Actions.numberAllContainers();

      $('#printdialog').addClass(HIDDEN_CLASS);


    });
  }
}

class Actions {

  static activateSaveIcon(element) {
    element.closest('.container3').find('>.accordion-header .save').removeClass(HIDDEN_CLASS);
    element.closest('.container2').find('>.accordion-header .save').removeClass(HIDDEN_CLASS);
    element.closest('.container1').find('>.accordion-header .save').removeClass(HIDDEN_CLASS);

    $('.actionbar .save').addClass("tosave");
  }

  static addNewJournal() {
    // Neues Journal hinzufügen
    console.log("Journal wird hinzugefügt");

    let template = $('.C_JOURNAL.template');
    let newJournal = template
      .clone()
      .removeClass("template")
      .prependTo(template.parent());

    newJournal.removeClass(DONTPRINT_CLASS);
    newJournal.addClass(PRINT_CLASS);

    return newJournal;
  }

  static entscheidClicked(element) {
    // je nach ausgewähltem Entscheid die entsprechenden Felder einblenden
    let feedbackitem = element.closest('.feedbackitem');
    let container = feedbackitem.closest('.accordion-body');
    let entscheid = feedbackitem
      .find('.radioitem.selected')
      .data("value");

    switch (entscheid) {
      case "tauglich":
        container.find('.E_HELPERS').removeClass('fi-hidden');
        container.find('.E_VERKP').addClass('fi-hidden');
        container.find('.E_LIMITATIONS').addClass('fi-hidden');
        break;
      case "untauglich":
        container.find('.E_HELPERS').addClass('fi-hidden');
        container.find('.E_VERKP').addClass('fi-hidden');
        container.find('.E_LIMITATIONS').addClass('fi-hidden');
        break;
      case "bedingt tauglich":
        container.find('.E_HELPERS').removeClass('fi-hidden');
        container.find('.E_VERKP').removeClass('fi-hidden');
        container.find('.E_LIMITATIONS').removeClass('fi-hidden');
        break;
      default:
        container.find('.E_HELPERS').addClass('fi-hidden');
        container.find('.E_VERKP').addClass('fi-hidden');
        container.find('.E_LIMITATIONS').addClass('fi-hidden');
        break;
    }
  }

  static timeMeasure(element) {

    var timefield = element
      .closest('.accordion-body')
      .find('.J_DURATION input');

    var secondsCounter = 0;
    var interval = window.setInterval(function () {
      secondsCounter++;
      var minutes = Math.floor(secondsCounter / 60.0);
      var seconds = secondsCounter - 60 * minutes;
      if (seconds < 10)
        seconds = '0' + seconds;
      //      timefield.val(minutes.toString() + ':' + seconds);
      timefield.val(minutes.toString() + ' min - automatisch');
    }, 1000);

  }

  static timeMesureStopp(element) {
    alert('Zeitmessung wird gestoppt');
    var timefield = element
      .closest('.accordion-body')
      .find('.J_DURATION input');

    timefield.change(function () {
      window.clearInterval(interval);
    });
  }

  static numberAllContainers() {


    $('.dontprint .accordion-header .posNum').text("");

    $('.container1.print').each(function (index1, element1) {

      var container1 = $(element1);
      let num1 = index1 + 1;

      container1.find('> .accordion-header .posNum').text(num1);

      container1.find('.container2.print').each(function (index2, element2) {

        var container2 = $(element2);
        let num2 = index2 + 1;

        container2.find('> .accordion-header .posNum').text(num1 + "." + num2);

      });

    });
  }

  static saveButtonClicked(element) {

    $('.edit').each(function (index, element) {
      var edit = $(element);
      var read = edit.next();

      if (edit.hasClass('radio')) {
        var newText = edit.find('input:checked').val();
        if (!newText) newText = '';
        newText = newText.trim();
        read.find('.pos1').text(newText);

      } else if (edit.hasClass('mitarbeiter')) {
        var newText = edit.find('select option:selected').text();
        read.find('p').text(newText);

      } else {
        var newText = edit.find('input').val();
        read.find('p').text(newText);
      }

    })
  }

  static togglePrintMarker(jqElement) {

    let wasPrint = jqElement.find('i').hasClass(PRINT_ICON);


    jqElement.find('i').toggleClass(PRINT_ICON);
    jqElement.find('i').toggleClass(DONTPRINT_ICON);

    jqElement.closest(".accordion").toggleClass(DONTPRINT_CLASS);
    jqElement.closest(".accordion").toggleClass(PRINT_CLASS);

    if (wasPrint) {
      jqElement
        .closest(".accordion")
        .find(".accordion .print i")
        .addClass(HIDDEN_CLASS);
    } else {
      jqElement
        .closest(".accordion")
        .find(".accordion .print i")
        .removeClass(HIDDEN_CLASS);
    }

  }
}

class Containers {

  static initIcons() {
    // show compress and expand only if there are subcontainers

    console.log("InitIcon läuft");

    $('.container1').each((index, element) => {
      let container = $(element);

      if (container.find(".container2").length > 0) {
        return;
      }

      container.find('.compress').addClass(HIDDEN_CLASS);
      container.find('.expand').addClass(HIDDEN_CLASS);
    });
  }

  static closeAllAccordions(start) {
    //    $('.content').find('.accordion').removeClass("open");
    start.find('.accordion').removeClass("open");
  }

  static closeAllJournalContainer() {
    //    debugger;
    $(".container2.C_JOURNAL").removeClass(OPEN_CLASS);

    App.setContainerTitle();
  }

  static openAllAccordions(start) {
    start.find('.accordion').addClass("open");
  }

  static setJournalTitle(jqJournalContainer) {

    var txtdatum = jqJournalContainer.find(".J_DATE input").val();

    let datum = new Date(txtdatum);

    var Tag = datum.getDate();
    var Monat = datum.getMonth() + 1;
    var Jahr = datum.getFullYear();

    let datumFormatted = Tag + "." + Monat + "." + Jahr;

    let titel = jqJournalContainer.find(".J_TITEL input").val();

    jqJournalContainer.find('.accordion-header .pos1').text(datumFormatted);
    jqJournalContainer.find('.accordion-header .pos2').text(titel);
  };

  static toggleAccordion(accordion) {
    accordion.toggleClass("open");
  }

  static openAccordion(accordion) {
    accordion.addClass("open");
  }

  static closeAccordion(accordion) {
    accordion.removeClass("open");
  }

  static toggleEditModus(element) {
    let icon = element;
    let accordion = icon.closest('.accordion');
    let isReadMode = accordion.hasClass(READ_CLASS);

    if (isReadMode) {
      Containers.toEdit(accordion);
    } else {
      Containers.toRead(accordion);
    }
  }

  static toRead(element) {
    element.closest('.accordion').removeClass(EDIT_CLASS);
    element.closest('.accordion').addClass(READ_CLASS);

    element.find('.accordion-body .accordion').removeClass(EDIT_CLASS);
    element.find('.accordion-body .accordion').addClass(READ_CLASS);
  }

  static toEdit(element) {
    element.closest('.accordion').removeClass(READ_CLASS);
    element.closest('.accordion').addClass(EDIT_CLASS);
    element.closest('.accordion').addClass(OPEN_CLASS);

    element.find('.accordion-body .accordion').removeClass(READ_CLASS);
    element.find('.accordion-body .accordion').addClass(EDIT_CLASS);
    element.find('.accordion-body .accordion').addClass(OPEN_CLASS);
  }

  static toPrint(element) {

    if (element.hasClass('template')) {
      return;
    }
    element.removeClass(DONTPRINT_CLASS);
    element.addClass(PRINT_CLASS);

    element.find('li.print i').removeClass(DONTPRINT_ICON);
    element.find('li.print i').addClass(PRINT_ICON);
  }

  static toNotPrint(element) {
    element.addClass(DONTPRINT_CLASS);
    element.removeClass(PRINT_CLASS);

    element.find('li.print i').addClass(DONTPRINT_ICON);
    element.find('li.print i').removeClass(PRINT_ICON);

    Containers.closeAccordion(element);
  }
}

class FormElements {

  static addFeebackItemReadValue(element, value1, value2) {

    element.closest('.values').find('.read .pos1, .read p').text(value1);
    element.closest('.values').find('.read .pos3').text(value2);
    element.closest('.feedbackitem').removeClass("novalue");
  };

  static radioElementClicked(radio) {

    let wasSelected = radio.hasClass(SELECTED_CLASS);

    radio
      .parent()
      .find(".radioitem")
      .removeClass(SELECTED_CLASS);

    if (!wasSelected) {
      radio.addClass(SELECTED_CLASS);
    }
  }

  static setJournalFieldTitle(target) {
    //Wenn Tätigkeit geklickt, dies in Titel eintragen, falls dieser nicht bereits von Hand editiert wurde

    var newText = target.text();
    if (!newText) newText = '';
    newText = newText.trim();
    let titleInput = target
      .closest('.accordion-body')
      .find('.J_TITEL input');

    if (titleInput.data(DATA_MANUALLY_EDITED))
      return;

    titleInput.val(newText);

  };

  static setReadValueRadioItem() {
    $(document.body).on("click", ".radioitem", event => {
      let target = $(event.currentTarget);
      var newText = target.text();
      //      debugger;

      if (newText == null) {
        newText = '';
      }

      newText = newText.trim();

      FormElements.addFeebackItemReadValue(target, newText);
    });

  }

  static setReadValueTextfield() {
    $(document.body).on("change", "input[type=text]", event => {
      let target = $(event.currentTarget);
      var newText = target.val();

      if (!newText) newText = '';
      newText = newText.trim();

      FormElements.addFeebackItemReadValue(target, newText);

    });
  }

}

class GuiElements {
  static showList(element) {
    element
      .find('.hiddenlist')
      .toggleClass("hidden");
  }

  static changeListValue(element, target) {

    let newText = element.text();
    if (!newText) newText = '';
    newText = newText.trim();

    target.text(newText);
  }
}

class Navigation {

  static scrollToTarget(jqTarget) {
    if (jqTarget.length == 0) {
      console.warn("jqTarget ist leer!", jqTarget);
      return;
    }

    let nativeTarget = jqTarget[0];
    const SCROLL_DURATION = 500;
    const SCROLL_TOP_CORRECTION = -50;

    $("main").animate({
      scrollTop: nativeTarget.offsetTop + SCROLL_TOP_CORRECTION,
    }, SCROLL_DURATION);
  }
}

$(document).ready(() => App.main());
