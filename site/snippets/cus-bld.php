<div class="value edit">
  <div class="th zeit">Uhrzeit</div>
  <div class="th druck">Blutdruck</div>
  <div class="th methode">Methode</div>
  <div class="th lage">Lage</div>
  <div class="th seite">Seite</div>
  <input type="time" class="td  zeit" name="zeit" value="08:00">
  <input type="text" class="td druck" name="druck" step="0.5" value="130/90">
  <div class="radiogroup methode">
    <div class="radioitem">manuell</div>
    <div class="radioitem">elektronisch</div>
  </div>

  <div class="radiogroup lage">
    <div class="radioitem">sitzend</div>
    <div class="radioitem">liegend</div>
  </div>

  <div class="radiogroup seite">
    <div class="radioitem">links</div>
    <div class="radioitem">rechts</div>
  </div>

  <div class="value read protected">
    <div class="th zeit">Uhrzeit</div>
    <div class="th druck">Blutdruck</div>
    <div class="th kommentar">Kommentar</div>
    <!--  Zeile 1-->
    <div class="td  zeit">08:00</div>
    <div class="td druck">130/90</div>
    <div class="td kommentar"><i class="far fa-trash"></i></div>

    <!--Zeile 2-->
    <div class="td zeit">08:00</div>
    <div class="td druck">130/90</div>
    <div class="td kommentar">elektronisch, liegend, links<i class="far fa-trash"></i></div>
  </div>

</div>
<div class="value read">
  <div class="th zeit">Uhrzeit</div>
  <div class="th druck">Blutdruck</div>
  <div class="th kommentar">Kommentar</div>
  <!--  Zeile 1-->
  <div class="td  zeit">08:00</div>
  <div class="td druck">130/90</div>
  <div class="td kommentar"></div>

  <!--Zeile 2-->
  <div class="td zeit">08:00</div>
  <div class="td druck">130/90</div>
  <div class="td kommentar">elektronisch, liegend, links</div>
</div>
