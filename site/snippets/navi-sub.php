<nav class="main-subnav">
  <ul>
    <?php 
    $children = $page->children();
    foreach($children as $child): ?>
    <li><a href="#<?= $child->uid() ?>">
        <?= $child->title() ?></a>
    </li>
    <?php endforeach ?>
  </ul>
  <ul class="display">
    <li><i class="compress fas fa-compress-alt"></i></li>
    <li><i class="expand fas fa-expand-alt"></i></li>
  </ul>
</nav>
