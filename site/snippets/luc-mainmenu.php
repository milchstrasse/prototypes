<?php $menu=r($page->uri()=="luc","display-full","display-small") ?>
<nav class="mainnav <?= $menu ?>">
  <div class="menu-toggle" title="Menu anzeigen / schliessen">
    <i class="fas fa-bars fa-fw"></i>
    <i class="fas fa-angle-left fa-fw"></i>
  </div>

  <ul class="topnav">
    <?php foreach($site->find('luc')->children()->visible() as $section){
      if($section->hasVisibleChildren() AND $section->subpages()=="list"){
        $listChildren=true;
      }else{
        $listChildren=false;
      }
    ?>

    <li class="<?= $section->color() ?> <?= e($listChildren,'hasChildren closed')?>">
      <a href="<?= e($listChildren,'#',$section->url()) ?>" title="
        <?= $section->title() ?>">

        <span>
          <?= e($listChildren,"<i class='fas fa-chevron-right fa-fw closed'></i><i class='fas fa-chevron-down fa-fw open'></i>") ?>
          <?= $section->title() ?>
        </span>
        <?php  ($section->icon()->isNotEmpty())?($icon=$section->icon()):($icon="arrow-square-right") ?>
        <i class="fas fa-<?= $icon ?> fa-fw icon"></i>
        <i class="fas fa-chevron-down fa-fw open"></i>
        <i class="fas fa-chevron-right fa-fw closed"></i>
      </a>
    </li>
    <?php if($listChildren): ?>
    <ul class="subnav closed">
      <?php $children = $section->children()->visible(); 
      foreach($children as $child): ?>
      <li class="<?= $section->color() ?>"><a href=" <?=$child->url() ?>" title="
        <?= $child->title() ?>">
          <span>
            <?= $child->title() ?></span>
          <?php if($child->icon()->isNotEmpty()?($icon=$child->icon()):($icon="arrow-square-right")) ?>
          <i class="fas fa-<?= $icon ?> fa-fw"></i>
        </a>
      </li>
      <?php endforeach ?>
    </ul>
    <?php endif ?>
    <?php }  // Ende Foreach Hauptmenu ?>
  </ul>
</nav>
