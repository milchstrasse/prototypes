<?php 
$i=$GLOBALS['step']; 
$m=$GLOBALS['modus']; 

$m="read-modus"; // immer im Readmodus laden, edit-modus wird per JS gesetzt
$template=r($container->istemplate()->isTrue(),"template","");
$printing=r($container->istemplate()->isTrue(),"dontprint","print");
$values=r(!$container->novalues()->isTrue()," hasvalues", "" );
// im Journal und bei Diagnosen die Container geschlossen anzeigen
switch($container->sourceid()){
  case "C_JOURNAL":
    $visibility = "closed";
    $piloticon="";
    break;
  case "DIAGNOSE":
    $visibility = "closed";
    $piloticon = "<i class='far fa-grip-lines'></i>";
    break;
  default:
    $visibility = "open";
    $piloticon="";
    break;
}
?>
<div id="<?= $container->uid() ?>" class="accordion container<?= $i." ".$m." ".$template." ".$visibility." ".$container->sourceid()." ".$printing ?>">
  <div class="accordion-header">
    <div class="counter">
    </div>
    <div class="pospilot">
      <?= $piloticon ?>
    </div>
    <div class="posNum"></div>
    <div class="pos1">
      <?= $container->title()->html() ?>
    </div>
    <div class="pos2">
      <?php e($container->novalues()->isTrue(),"<span class='markervalue'>keine Werte vorhanden</span>")?>
    </div>
    <div class="pos3">
    </div>
    <ul class="actions">
      <li class="save hidden"><i class=" fas fa-save"></i></li>
      <li class="modus"><i class=" fas fa-pencil"></i></li>
      <li class="print"><i class="fas fa-print fa-fw"></i></li>
      <?php if($i==1): ?>
      <li class="compress"><i class="fas fa-compress-alt"></i></li>
      <li class="expand"><i class="fas fa-expand-alt"></i></li>
      <?php endif ?>
      <li class="toggle"><i class=" fas fa-plus"></i></li>
    </ul>
  </div>
  <div id="demo2" class="accordion-body">
    <!--    first execute the individual snippet-->
    <?php
    if($container->customsnippet()!=""){
    snippet($container->customsnippet(), array('fiitem' =>$container));
    }
      ?>
    <!--    Show all feedbackitems from structure -->
    <?php 
      foreach($container->fiitems()->toStructure() as $fiitem){ 
        snippet('feedbackitem', array('fiitem' =>$fiitem));
      }
    ?>

    <!--Show all other childrens in own containers-->
    <?php 
    
    $GLOBALS['step'] =$GLOBALS['step']+1;
    
    foreach($container->children()->visible()  as $fiitem){
      if ($fiitem->intendedTemplate()!='feedbackitem'){
        snippet('container', array('container' =>$fiitem));
      }
          
      }
    $GLOBALS['step'] =$GLOBALS['step']-1;
  ?>

  </div>
</div>
