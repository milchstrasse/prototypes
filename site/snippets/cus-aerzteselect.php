<div class="value edit">
  <select class="pos1" name="assignedTo">
    <option></option>
    <option value="UNASSIGNED">Nicht zugewiesen</option>
    <option value="">Bürgin, Esther </option>
    <option value="">De Greco, Raphaele </option>
    <option value="">Dubois, Daniel </option>
    <option value="">Feuerbacher, Nathalie </option>
  </select>
</div>
<div class="value read">
  <div class="pos1">
    <?= e($item->value()->isNotEmpty(),$item->value(),"") ?>
  </div>
  <div class="pos2">
    <?= e($item->value2()->isNotEmpty(),$item->value2(),"") ?>
  </div>
</div>
