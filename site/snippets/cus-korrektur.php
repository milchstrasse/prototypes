<div class="value edit">
  <div class="thc re">Rechts</div>
  <div class="thc li">Links</div>
  <div class="thr sph">Sph</div>
  <div class="thr cyl">Cyl</div>
  <div class="thr add">Add</div>
  <input type="number" class="re sph" min="-12" max="+12" step="0.25">
  <input type="number" class="re cyl" min="-12" max="+12" step="0.25">
  <input type="number" class="re add" min="-12" max="+12" step="0.25">
  <input type="number" class="li sph" min="-12" max="+12" step="0.25">
  <input type="number" class="li cyl" min="-12" max="+12" step="0.25">
  <input type="number" class="li add" min="-12" max="+12" step="0.25">
</div>
<div class="value read">
  <div class="thc re">Rechts</div>
  <div class="thc li">Links</div>
  <div class="thr sph">Sph</div>
  <div class="thr cyl">Cyl</div>
  <div class="thr add">Add</div>
  <div class="td re sph">-5.0</div>
  <div class="td re cyl">-0.75</div>
  <div class="td re add"></div>
  <div class="td li sph">-5.5</div>
  <div class="td li cyl">-1.0</div>
  <div class="td li add"></div>
</div>
