<header class="mainheader">
  <div class="wobinich">
    <?php if($page->bereich()=="luc" AND $page->uri()!="luc" ){
            $home=$site->page('luc')->url();
          }else{
            $home=$site->homePage()->url();
          } 
    ?>
    <a href="<?=$home ?>">
      <i class="fas fa-<?= $site->version() ?> fa-fw tohome">
      </i>
      <i class="fas fa-home fa-fw fakehome">
      </i>
    </a>
    <p>
      <?= $page->title() ?>
    </p>
  </div>

  <div class="choseVersion">
    <div id="version">
      <p>Standard</p>
      <ul class="hiddenlist hidden">
        <li data-version="standard">Standard</li>
        <li data-version="variante1">Variante 1 (Felder ohne Werte verstecken)</li>
        <li data-version="variante2">Variante 2 (Layout 2)</li>
      </ul>
    </div>
    <div id="benutzer">
      <p>Esther Bürgin (Ärztin)</p>
      <i class="fas fa-chevron-square-down"></i>
      <ul class="hiddenlist hidden">
        <li data-user="Esther" data-role="Arzt">Esther Bürgin (Ärztin)</li>
        <li data-user="Daniel" data-role="QM-Arzt">Daniel Dubois (QM-Arzt)</li>
        <li data-user="Benno" data-role="CaseManager">Benno Müller (Case-Manager)</li>
        <li data-user="Rosemarie" data-role="BackOffice">Rosemarie Albisser (Backoffice)</li>
      </ul>
    </div>
  </div>

</header>
