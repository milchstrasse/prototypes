<div class="value edit">
  <div class="th main senkrecht">Anomaloskop</div>
  <div class="th v5">M</div>
  <div class="th v6">AQ</div>
  <div class="r15 th senkrecht">R15</div>
  <div class="r13 th senkrecht">R13</div>
  <div class="r12 th senkrecht">R12</div>
  <input class="td r15 v1" type="number">
  <input class="td r15 v2" type="number">
  <input class="td r15 v3" type="number">
  <input class="td r15 v4" type="number">
  <div class="readonly td r15 v5">48.375</div>
  <div class="readonly td r15 v6"></div>
  <input class="td r13 v1" type="number">
  <input class="td r13 v2" type="number">
  <input class="td r13 v3" type="number">
  <input class="td r13 v4" type="number">
  <div class="readonly td r13 v5"></div>
  <div class="readonly td r13 v6"></div>
  <input class="td r12 v1" type="number">
  <input class="td r12 v2" type="number">
  <input class="td r12 v3" type="number">
  <input class="td r12 v4" type="number">
  <div class="readonly td r12 v5"></div>
  <div class="readonly td r12 v6"></div>
</div>
<div class="value read">
  <div class="th main th senkrecht">Anomaloskop</div>
  <div class="th main v5">M</div>
  <div class="th main v6">AQ</div>
  <div class="r15 th senkrecht">R15</div>
  <div class="r13 th senkrecht">R13</div>
  <div class="r12 th senkrecht">R12</div>

  <div class="td r15 v1">45.5</div>
  <div class="td r15 v2">50.5</div>
  <div class="td r15 v3">49</div>
  <div class="td r15 v4">48.5</div>
  <div class="td r15 v5">48.375</div>
  <div class="td r15 v6">0.62</div>
  <div class="td r13 v1">48.5</div>
  <div class="td r13 v2">50</div>
  <div class="td r13 v3">46.5</div>
  <div class="td r13 v4">50</div>
  <div class="td r13 v5">48.75</div>
  <div class="td r13 v6">0.6</div>
  <div class="td r12 v6"></div>
  <div class="td r12 v1"></div>
  <div class="td r12 v2"></div>
  <div class="td r12 v3"></div>
  <div class="td r12 v4"></div>
  <div class="td r12 v5">0</div>
  <div class="td r12 v6"></div>
</div>
