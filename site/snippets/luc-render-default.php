<!--Show all children in own sections -->
<?php $GLOBALS['step'] = "0"; ?>
<?php foreach($page->children() as $section){
    switch($section->intendedTemplate()){
      case "container";
        $GLOBALS['step'] = "1";
        snippet('container', array('container' => $section));
        break;
        
      case "js-rendering";
        $GLOBALS['step'] = "1";
        snippet('js-rendering', array('container' => $section));
        break;
        
      default:
        snippet('defaultcontent', array('container' => $section));
        break;
      }
    }
  ?>
