<div class="value edit">
  <div class="th zeit">Uhrzeit</div>
  <div class="th druck">Blutdruck</div>
  <div class="th kommentar">Kommentar</div>
  <div class="td  zeit"><input type="time" name="zeit" value="08:00"></div>
  <div class="td druck"><input type="text" name="druck" step="0.5" value="130/90"></div>
  <div class="td kommentar"><input type="text" name="kommentar"></div>
  <div class="td  zeit"><input type="time" name="zeit" value="08:30"></div>
  <div class="td druck"><input type="text" name="druck" step="0.5" value="159/100"></div>

  <div class="td kommentar"><input type="text" name="kommentar" value="elektronisch, liegend, links"></div>
</div>
<div class="value read">
  <div class="th zeit">Uhrzeit</div>
  <div class="th druck">Blutdruck</div>
  <div class="th kommentar">Kommentar</div>
  <!--  Zeile 1-->
  <div class="td  zeit">08:00</div>
  <div class="td druck">130/90</div>
  <div class="td kommentar"></div>

  <!--Zeile 2-->
  <div class="td zeit">08:00</div>
  <div class="td druck">130/90</div>
  <div class="td kommentar">elektronisch, liegend, links</div>
</div>
