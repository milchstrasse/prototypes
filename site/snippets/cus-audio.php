<div class="value edit">
  <div class="th main thr"></div>
  <div class="th main v1">0.5</div>
  <div class="th main v2">1</div>
  <div class="th main v3">2</div>
  <div class="th main v4">3</div>
  <div class="th main v5">4</div>
  <div class="th main v6">6</div>
  <div class="th main v7">8 kHZ</div>

  <div class="re th senkrecht">Rechts</div>
  <input class="re v1" type="number" value="10">
  <input class="re v2" type="number" value="25">
  <input class="re v3" type="number" value="15">
  <input class="re v4" type="number" value="25">
  <input class="re v5" type="number" value="25">
  <input class="re v6" type="number" value="40">
  <input class="re v7" type="number" value="60">

  <div class="li th senkrecht">Links</div>
  <input class="li v1" type="number" value="10">
  <input class="li v2" type="number" value="20">
  <input class="li v3" type="number" value="35">
  <input class="li v4" type="number" value="25">
  <input class="li v5" type="number" value="30">
  <input class="li v6" type="number" value="25">
  <input class="li v7" type="number" value="30">
</div>
<div class="value read">
  <div class="th main thr"></div>
  <div class="th main v1">0.5</div>
  <div class="th main v2">1</div>
  <div class="th main v3">2</div>
  <div class="th main v4">3</div>
  <div class="th main v5">4</div>
  <div class="th main v6">6</div>
  <div class="th main v7">8 kHZ</div>

  <div class="re th senkrecht">Rechts</div>
  <div class="td re v1">10</div>
  <div class="td re v2">25</div>
  <div class="td re v3">15</div>
  <div class="td re v4">25</div>
  <div class="td re v5">25</div>
  <div class="td re v6">40</div>
  <div class="td re v7">60</div>

  <div class=" li th senkrecht">Links</div>
  <div class="td li v1">10</div>
  <div class="td li v2">20</div>
  <div class="td li v3">35</div>
  <div class="td li v4">25</div>
  <div class="td li v5">30</div>
  <div class="td li v6">25</div>
  <div class="td li v7">30</div>
</div>
