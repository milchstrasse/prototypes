<div class="value edit nurqmarzt">
  <div class="mediumemphasis button modalopen ">Entscheid bestätigen</div>
</div>
<div class="value protected feedback hidden">
  <p class="pos1 response">
    Entscheid bestätigt
  </p>
  <p class="pos9 date">
    22.01.2019
  </p>
  <p class="pos5 name">
    Dr. med. Daniel Dubois
  </p>
  <p class="pos12 icon delete nurqmarzt">
    <i class="far fa-trash"></i>
  </p>
</div>

<div class="main modal medium hidden">
  <div class="window">
    <div class="header">
      <h1>Qualitätsmanagement: Entscheid bestätigen</h1>
    </div>
    <div class="message">
      <p>Folgender Entscheid wird bestätigt:</p>
      <p><em>BAV 2c</em>für <em>Samuel Heidegger</em></p>
      <p>Entscheid: <em>tauglich</em></p>
    </div>
    <div class="action">
      <div class="button highemphasis confirm">Bestätigen</div>
      <div class="button lowemphasis abort">Abbrechen</div>
    </div>
  </div>
</div>

<div class="modal delete small hidden">
  <div class="window">
    <div class="header">
      <h1>Qualitätsmanagement: Entscheid löschen</h1>
    </div>
    <div class="message">
      <p>Wollen Sie die Bestätigung wieder entfernen?</p>
    </div>
    <div class="action">
      <div class="button highemphasis confirm">Entfernen</div>
      <div class="button lowemphasis abort">Abbrechen</div>
    </div>
  </div>
</div>
