<div class="value edit">
  <?php $span="4"; ?>
  <div class="radioitem 4" data-value="E66" style="grid-column: span <?= $span ?>">
    Adipositas
  </div>
  <div class="radioitem 4" data-value="I95" style="grid-column: span <?= $span ?>">
    Hypotonie
  </div>
  <div class="radioitem 4" data-value="G71" style="grid-column: span <?= $span ?>">
    Myopie
  </div>
  <div class="radioitem 4" data-value="H52.2" style="grid-column: span <?= $span ?>">
    Astigmatismus
  </div>
  <div class="radioitem 4" data-value="H52.4" style="grid-column: span <?= $span ?>">
    Presbyopie
  </div>
  <div class="radioitem 4" data-value="I27" style="grid-column: span <?= $span ?>">
    pulmonale Herzkrankheiten
  </div>
  <div class="radioitem 4" data-value="F45" style="grid-column: span <?= $span ?>">
    Somatoforme Störungen
  </div>
  <div class="radioitem 4" data-value="E80.4" style="grid-column: span <?= $span ?>">
    Gilbert-Meulengracht-Syndrom
  </div>
  <div class="radioitem 4" data-value="N20" style="grid-column: span <?= $span ?>">
    Nieren- und Ureterstein
  </div>
  <div class="icd10suche" style="grid-column: span <?= $span ?>">
    <input type="text" placeholder="ICD 10 Suche" value="<?= $item->value() ?>">
  </div>
  <div class="texticd10" style="grid-column: span <?= (12-$span) ?>">
    <?= $item->value2() ?>
  </div>
</div>
<div class="value read">
  <div class="pos1">
    <?= e($item->value()->isNotEmpty(),$item->value(),"") ?>
  </div>
  <div class="pos2">
    <?= e($item->value2()->isNotEmpty(),$item->value2(),"") ?>
  </div>
</div>
