<div class="value edit">
  <div class="th bmi">BMI</div>
  <div class="th groesse">Grösse (cm)</div>
  <div class="th gewicht">Gewicht (kg)</div>
  <div class="th bemerkung">Bemerkung</div>
  <div class="td bmi">25.6</div>
  <input class="td  groesse" type="number" min="100" max="250" step="0.5" name="groesse" value="169">
  <input class="td gewicht" type="number" name="gewicht" min="30" max="250" step="0.5" value="73">
  <input type="text" class="td bemerkung" name="bemerkung">
</div>
<div class="value read">
  <div class="th bmi">BMI</div>
  <div class="th groesse">Grösse (cm)</div>
  <div class="th gewicht">Gewicht (kg)</div>
  <div class="th bemerkung">Bemerkung</div>

  <div class="td bmi">25.6</div>
  <div class="td  groesse">169</div>
  <div class="td gewicht">73</div>
  <div class="td bemerkung"></div>
</div>
