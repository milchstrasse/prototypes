<div class="value edit">
  <div class="th main name">Name des Medikamentes</div>
  <div class="th main einh">Einheit</div>
  <div class="th main dos">Dosierung</div>
  <div class="th main bem">Bemerkung</div>
  <div class="th main seit">Verordnet seit:</div>

  <div class="th sub mo">morgens</div>
  <div class="th sub mi">mittags</div>
  <div class="th sub ab">abends</div>
  <div class="th sub na">nachts</div>

  <input class="td name" type="text">
  <input class="td einh" type="text">
  <input class="td mo" type="text">
  <input class="td mi" type="text">
  <input class="td ab" type="text">
  <input class="td na" type="text">
  <input class="td bem" type="text">
  <input class="td seit" type="date">
</div>
<div class="value read">
  <div class="th main name">Name des Medikamentes</div>
  <div class="th main einh">Einheit</div>
  <div class="th main dos">Dosierung</div>
  <div class="th main bem">Bemerkung</div>
  <div class="th main seit">Verordnet seit:</div>

  <div class="th sub mo">morgens</div>
  <div class="th sub mi">mittags</div>
  <div class="th sub ab">abends</div>
  <div class="th sub na">nachts</div>

  <div class="td name">Belok ZOK</div>
  <div class="td einh">200mg</div>
  <div class="td mo">1</div>
  <div class="td mi"></div>
  <div class="td ab"></div>
  <div class="td na"></div>
  <div class="td bem"></div>
  <div class="td seit">----</div>

  <div class="td r2 name">Lisinopril </div>
  <div class="td r2 einh">20mg</div>
  <div class="td r2 mo">1</div>
  <div class="td r2 mi"></div>
  <div class="td r2 ab"></div>
  <div class="td r2 na"></div>
  <div class="td r2 bem"></div>
  <div class="td r2 seit">----</div>
</div>
