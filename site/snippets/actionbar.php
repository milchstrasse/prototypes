<aside class="actionbar">
  <ul>
    <li>
      <i class="modus read-modus fas fa-pencil"></i>
    </li>
    <li>
      <i class="save fas fa-save"></i>
    </li>
    <li>
      <i class="help far fa-question-circle"></i>
    </li>
    <li>
      <i class="journal far fa-book"></i>
    </li>
    <li>
      <a href="<?= $site->page('sonderseiten/prints/ub1')->url() ?>">
        <i class="print far fa-print"></i>
      </a>
    </li>
  </ul>
</aside>
