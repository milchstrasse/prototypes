<!-- zeigt alle Feedbackitems. Übergabe als fiitem -->
<?php
  $TXT_NOVALUE="keine Werte vorhanden";

  $sid=$fiitem->sourceid();
  $type=r($fiitem->fitype()=='ficustom', $fiitem->ficustom(),$fiitem->fitype());
  $value=r($fiitem->value()->isNotEmpty(),'hasvalue','novalue');
  $sortable=r($fiitem->sortable(), "sortable","");
  $display=r($fiitem->hidden()->isTrue(), "fi-hidden","");
?>

<div class='feedbackitem <?= $value." ".$sid." ".$display ?> '>
  <div id="<?= $sid ?>" class="maingrid <?= $type ?>">
    <div class="infos">
      <?php snippet('fi-help', array('fiitem' => $fiitem)) ; ?>
      <?php snippet('fi-hint', array('fiitem' => $fiitem)) ; ?>
    </div>
    <label class="label <?= e($fiitem->reqfield()=='1','reqfield') ?> <?= e($fiitem->help()->isNotEmpty(),'hastooltipp') ?>" for="
      <?= $fiitem->sourceid() ?>">
      <?= $fiitem->label()->html() ?>
      <?php snippet('fi-help-tooltipp', array('fiitem' => $fiitem)) ; ?>
    </label>
    <div class="values">
      <?php if($fiitem->fitype()=="ficustom"): ?>

      <?php snippet($type, array('item' => $fiitem)) ; ?>

      <?php else: ?>

      <div class="value edit">
        <?php snippet($type, array('item' => $fiitem)) ; ?>
      </div>
      <div class="value read">
        <div class="pos1">
          <?= e($fiitem->value()->isNotEmpty(),$fiitem->value(),$TXT_NOVALUE) ?>
        </div>
        <div class="pos3">
          <?= e($fiitem->value2()->isNotEmpty(),$fiitem->value2(),"") ?>
        </div>
      </div>

      <?php endif ?>
    </div>
  </div>
</div>
