<div class="cards">
  <div class="card booking">
    <div class="logo">
      <i class="far fa-calendar-day fa-fw"></i></div>
    <div class="content">
      <h1>Fachadmin Booking</h1>
      <ul>
        <li>Timeslots exportieren</li>
        <li>Timeslots importieren</li>
        <li>Klienten-Stammdaten importieren</li>
        <li>...</li>
      </ul>
    </div>
  </div>
  <div class="card finance">
    <div class="logo">
      <i class="far fa-usd-circle fa-fw"></i>
    </div>
    <div class="content">
      <h1>Fachadmin Finance</h1>
      <ul>
        <li>Zahlungsliste importieren</li>
        <li>SAP PLZ importieren</li>
        <li>Kundenstammdaten exportieren</li>
        <li>Kundenstammdaten importieren</li>
      </ul>
    </div>
  </div>
  <div class="card reports">
    <div class="logo">
      <i class="far fa-chart-line fa-fw"></i>
    </div>
    <div class="content">
      <h1>Reports</h1>
      <ul>
        <li>Zeiterfassung</li>
        <li>Jahresbericht</li>
        <li>...</li>
      </ul>
    </div>
  </div>
  <div class="card sysadmin">
    <div class="logo">
      <i class="far fa-tools fa-fw"></i>
    </div>
    <div class="content">
      <h1>Sysadmin</h1>
      <ul>
        <li>Grids</li>
        <li>Wartungsfenster bearbeiten</li>
        <li>...</li>
      </ul>
    </div>
  </div>
</div>
