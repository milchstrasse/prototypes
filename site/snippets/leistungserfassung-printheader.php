<div class="printheader printonly">
  <div class="printtitle">
    <h1>Untersuchungsbericht</h1>
    <div class="logo">
      <img src="<?= $site->image('logo_hms.svg')->url() ?>" class="logo">
    </div>
  </div>
  <div class="printmain">
    <ul class="infoClient">
      <li>
        <p class="label">Name, Vorname</p>
        <p class="values">Heidegger, Samuel</p>
      </li>
      <li>
        <p class="label">Geburtsdatum</p>
        <p class="values">25.10.1985</p>
      </li>
      <li>
        <p class="label">PLZ, Ort</p>
        <p class="values">3045 Bern</p>
      </li>
      <li>
        <p class="label">Funktion, Anforderungsstufe</p>
        <p class="values">Triebfahrzeugführender CH, AS1</p>
      </li>
    </ul>
    <ul class="infoUntersuchung">
      <li>
        <p class="label">Bezeichnung der Untersuchung</p>
        <p class="values">Gruppe 1 / Gruppe 2 - Periodische Beurteilung</p>
      </li>
      <li>
        <p class="label">Datum und Ort der Untersuchung</p>
        <p class="values">13.09.2018, Bern</p>
      </li>
    </ul>
  </div>
</div>
