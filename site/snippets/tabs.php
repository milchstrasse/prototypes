<header class="tabs">
  <nav class="tabs">
    <ul>
      <?php
      $siblings = $page->siblings();
      foreach($siblings as $sibling): ?>
      <li>
        <a <?php e($sibling->isActive(), ' class="active"') ?> href="
          <?= $sibling->url() ?>">
          <?= $sibling->title(); ?>
        </a></li>
      <?php endforeach ?>
    </ul>
  </nav>
</header>
