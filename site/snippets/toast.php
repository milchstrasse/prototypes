<div class="toast success hidden slide-in-bottom">
  <h1 class="title">Vorgang erfolgreich abgeschlossen</h1>
  <p class="message">
    Alles klar! Sie können weitermachen...
  </p>
</div>

<div class="toast warn hidden slide-in-bottom">
  <h1 class="title">Vorgang mit Fehlern abgeschlossen</h1>
  <p class="message">
    Folgendes konnte nicht wie erwartet gespeichert werden:<br>
    1.
  </p>
</div>

<div class="toast error hidden">
  <h1 class="title">Vorgang konnte nicht ausgeführt werden</h1>
  <p class="message">
    Bitte Informieren Sie die Hotline!
  </p>
</div>
