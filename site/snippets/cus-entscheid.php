<div class="value edit">
  <?php 
    $props = explode(",",$item->selectvalues());
    foreach ($props as $prop): ?>

  <?php $s = r($item->value()==$prop,"selected",""); ?>

  <div class="radioitem <?= $s ?>" style="grid-column: span <?= $item->span() ?>">
    <?= $prop  ?>
  </div>
  <?php endforeach ?>
</div>
<div class="value read">
  <div class="pos1">
    <?= e($item->value()->isNotEmpty(),$item->value(),"keine Werte vorhanden") ?>
  </div>
  <div class="pos2">
    <?= e($item->value2()->isNotEmpty(),$item->value2(),"") ?>
  </div>
</div>
