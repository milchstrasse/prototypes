<div id="printdialog" class="modal medium hidden">
  <div class="window">
    <div class="header">Druck Layout wählen</div>
    <div class="message">
      <p>Bitte wählen Sie das Drucklayout aus</p>
      <ul>
        <li class="button all">Alles</li>
        <li class="button untersuch">Untersuchungsergebnisse</li>
      </ul>
    </div>
    <div class="action">
      <ul>
        <li class="button">Abbrechen</li>
      </ul>
    </div>
  </div>
</div>
