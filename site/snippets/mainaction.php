<div class="maincontent">
  <?php if($page->custom()->isNotEmpty()):
    snippet($page->custom());
  ?>
  <?php elseif($page->text()->isNotEmpty()): ?>
  <div class="seitentitel">
    <h1>
      <?= $page->title()->html(); ?>
    </h1>
  </div>
  <p>
    <?= $page->text()->kirbytext(); ?>
  </p>
  <?php else: ?>
  <p>
    für diese Seite ist noch kein Inhalt definiert
  </p>
  <?php endif ?>
</div>
