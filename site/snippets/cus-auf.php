<div class="value header">
  <div class="th beginn">Beginn</div>
  <div class="th ende">Ende</div>
  <div class="th grad">Schweregrad</div>
  <div class="th verlauf">Verlauf</div>
</div>
<div class="value edit new">
  <input class="td beginn" type="date" name="beginn">
  <input class="td ende" type="date" name="ende">
  <input class="td grad" type="number" min="0" max="100" step="5" name="grad">
  <input class="td verlauf" type="text" name="verlauf">
</div>
<div class="tablevalues" id="EL_AUFB">
  <div class="value read">
    <div class="td beginn">15.06.2018</div>
    <div class="td ende">31.08.2018</div>
    <div class="td grad">3</div>
    <div class="td verlauf">Lorem Ipsum<i class="far fa-trash"></i></div>
  </div>
  <div class="value read protected">
    <div class="td beginn">30.10.2017</div>
    <div class="td ende">20.12.2017</div>
    <div class="td grad">5</div>
    <div class="td verlauf">Lorem Ipsum<i class="far fa-trash"></i></div>
  </div>
  <div class="value read protected">
    <div class="td beginn">05.05.2017</div>
    <div class="td ende">30.06.2017</div>
    <div class="td grad">2</div>
    <div class="td verlauf">Lorem Ipsum<i class="far fa-trash"></i></div>
  </div>
  <div class="value read protected">
    <div class="td beginn">02.04.2017</div>
    <div class="td ende">12.04.2017</div>
    <div class="td grad">2</div>
    <div class="td verlauf">Anderer Text<i class="far fa-trash"></i></div>
  </div>
  <div class="value read protected">
    <div class="td beginn">03.03.2017</div>
    <div class="td ende">15.03.2017</div>
    <div class="td grad">2</div>
    <div class="td verlauf">Schnell genesen<i class="far fa-trash"></i></div>
  </div>
</div>
<!--
<div class="subButton">
  <a href="#">Eintrag hinzufügen</a>
</div>
-->
