<div class="value edit">
  <div class="th main ferne">Ferne</div>
  <div class="th main naehe">Nähe</div>
  <div class="th sub links">Links</div>
  <div class="th sub rechts">Rechts</div>
  <div class="th sub binoc">Binoc</div>
  <div class="th senkrecht landolt">Landolt</div>
  <div class="th senkrecht e">E</div>
  <input class="landolt links" type="number" min="-12" max="+12" step="0.25">
  <input class="landolt rechts" type="number" min="-12" max="+12" step="0.25">
  <input class="landolt binoc" type="number" min="-12" max="+12" step="0.25">
  <input class="e links" type="number" min="-12" max="+12" step="0.25" value="1.0">
  <input class="e rechts" type="number" min="-12" max="+12" step="0.25" value="1.0">
  <input class="e binoc" type="number" min="-12" max="+12" step="0.15" value="0.4">
</div>
<div class="value read">
  <div class="th main ferne">Ferne</div>
  <div class="th main naehe">Nähe</div>
  <div class="th sub links">Links</div>
  <div class="th sub rechts">Rechts</div>
  <div class="th sub binoc">Binoc</div>
  <div class="th senkrecht landolt">Landolt</div>
  <div class="th senkrecht e">E</div>
  <div class="td landolt links"></div>
  <div class="td landolt rechts"></div>
  <div class="td landolt binoc">0.3</div>
  <div class="td e links">1.0</div>
  <div class="td e rechts">1.0</div>
  <div class="td e binoc"></div>
</div>
