<!DOCTYPE html>
<html lang="de">

<head>

  <meta charset="utf-8">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <!--<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">-->
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <?= css('assets/css/reset.css') ?>
  <?= css('assets/css/print.css') ?>
  <?= css('assets/css/all.css') // Fontawesome Icons, Lokal gehostet ?>
  <title>
    <?php echo $site->title()->html() ?> |
    <?php echo $page->title()->html() ?>
  </title>
  <link rel="shortcut icon" href="favicon.ico">
</head>

<body class="<?= $page->style() ?> preload">
