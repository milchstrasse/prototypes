<?php 
// Die XML-Strukturen für den aktuellen Tab als GLOBALS (mehrdimensionale Arrays) laden
//
// $GLOBALS['leistungsstruktur'] => die Originalsleistungsstruktur, die auch in LuC verwendet wird

// $GLOBALS['valueset'] => MockUp-Daten, inkl. Hinweise und Hilfe

snippet('xmlladen-tab');
?>

<?php snippet('head') ?>
<?php snippet('header') ?>
<?php snippet('luc-mainmenu') ?>
<?php snippet('infopanel') ?>
<?php snippet('navigation') ?>
<?php snippet('tabs') ?>
<?php snippet('seitentitel') ?>


<main>
  <?php snippet('xml-subnavi') ?>
  <?php snippet('actionbar') ?>

  <div class="content">
    <?php $GLOBALS['step']=0;
    switch($page->title()){
      case "Diagnosen":
        echo "<h3>Noch kein HiFi-Prototype vorhanden</h3>";
        break;
      case "Nachbearbeitung":
        echo "<h3>Noch kein HiFi-Prototype vorhanden</h3>";
        break;
      case "Rechnung":
        echo "<h3>Noch kein HiFi-Prototype vorhanden</h3>";
        break;
      default:
        foreach ($GLOBALS['leistungsstruktur']->Container as $container) {
          snippet('xml-renderer', array( 'container' => $container));
        }
        break;
    }

?>
  </div>
</main>

<?php snippet('toast') ?>
<?php snippet('footer') ?>
