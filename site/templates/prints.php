<?php snippet('head-print') ?>
<?php snippet('header') ?>
<aside class="infopanel">
  <div class="seitentitel">
    <h1>
      <?= $page->title()->html(); ?>
    </h1>
  </div>
</aside>

<main class="printarea">
  <div class="printheader">
    <h1>Untersuchungsbericht</h1>
    <div>
      <img src="<?= image('logo_hms.svg')->url() ?>" class="logo">
    </div>
  </div>
  <div class="printmain">
    <ul class="infoKlient">
      <li>
        <p class="label">Name, Vorname</p>
        <p class="values">Heidegger, Samuel</p>
      </li>
      <li>
        <p class="label">Geburtsdatum</p>
        <p class="values">25.10.1985</p>
      </li>
      <li>
        <p class="label">PLZ, Ort</p>
        <p class="values">3045 Bern</p>
      </li>
      <li>
        <p class="label">Funktion, Anforderungsstufe</p>
        <p class="values">Triebfahrzeugführender CH, AS1</p>
      </li>
    </ul>
    <ul class="infoUntersuchung">
      <li>
        <p class="label">Bezeichnung der Untersuchung</p>
        <p class="values">Gruppe 1 / Gruppe 2 - Periodische Beurteilung</p>
      </li>
      <li>
        <p class="label">Datum und Ort der Untersuchung</p>
        <p class="values">13.09.2018, Bern</p>
      </li>
    </ul>
  </div>
  <div class="content <?= $page->print() ?>">
    <?php $GLOBALS['modus']="read-modus" ?>
    <?php snippet('getcontent', array('start'=> $site->find('luc/auftraege/u/untersuchung'))) ?>
  </div>
</main>


<?php snippet('footer') ?>
