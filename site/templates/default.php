<?php if($page->bereich()=="luc"): ?>
<?php snippet('head') ?>
<?php snippet('header') ?>
<?php snippet('luc-mainmenu') ?>
<?php snippet('infopanel') ?>
<main>
  <?php snippet('mainaction') ?>
</main>


<?php snippet('footer') ?>

<?php else: ?>
<?php snippet('head') ?>
<main>
  <?php snippet('seitentitel') ?>
  <div>
    <?= $page->text()->kirbytext(); ?>
    <?= snippet($page->custom()); ?>
  </div>
</main>
<?php snippet('footer') ?>
<?php endif ?>
