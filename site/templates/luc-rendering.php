<?php snippet('head') ?>
<?php snippet('header') ?>
<?php snippet('luc-mainmenu') ?>
<?php snippet('infopanel') ?>
<?php snippet('tabs') ?>
<?php snippet('leistungserfassung-fixedheader') ?>
<?php snippet('seitentitel') ?>
<?php snippet('leistungserfassung-printheader') ?>

<?php $GLOBALS['modus'] = get('v')."-modus"; ?>


<main>

  <?php snippet('navi-sub') ?>
  <?php snippet('actionbar') ?>



  <div class="content">
    <?php snippet($page->rendermode()) ?>
  </div>
</main>

<?php snippet('modal-printselect') ?>
<?php snippet('toast') ?>
<?php snippet('footer') ?>
