<?php snippet('head') ?>
<main>
  <?php snippet('seitentitel') ?>
  <div>
    <?= $page->text()->kirbytext(); ?>
    <?= snippet($page->custom()); ?>
  </div>
</main>
<?php snippet('footer') ?>
