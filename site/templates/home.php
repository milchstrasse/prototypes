<?php snippet('head-home') ?>

<div class="right">
  <nav class="startseite">
    <div id="luc" class="accordion">
      <h1 class="accordion-header">LuC<i class="fas fa-caret-down"></i></h1>
      <ul class="accordion-body">
        <li class="card-1">
          <a href="
        <?= $pages->find('luc')->url() ?>">
            LuC-Startseite
          </a>
        </li>
        <li class="card-1">
          <a href="
        <?= $pages->find('luc/auftraege/u/diagnosen')->url() ?>">
            Diagnosen (aktiv/inaktiv, sortierbar)
          </a>
        </li>
        <li class="card-1">
          <a href="
        <?= $pages->find('luc/auftraege/u/entscheid')->url() ?>">
            Entscheid (Freigabe durch QM)
          </a>
        </li>
        <li class="card-1">
          <a href="
        <?= $pages->find('luc/auftraege/u/untersuchung')->url() ?>">
            Untersuchung (Leistungserfassung)
          </a>
        </li>
        <li class="card-1">
          <a href="
        <?= $pages->find('luc/auftraege/a/abklaerung')->url() ?>">
            Abklärung (Leistungserfassung)
          </a>
        </li>
        <li class="card-1">
          <a href="
        <?= $pages->find('luc-designpatterns')->url() ?>">
            Design-Patterns
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="credits">
    <p>
      <?= $page->credits()->kirbytext(); ?>
    </p>
  </div>
</div>

<main class="startseite">
  <h1 class="seitentitel">
    <?= $page->ueberschrift()->html(); ?>
  </h1>
  <div class="icon">
    <i class="fas fa-<?=$site->version() ?>"></i>
  </div>
  <div class="list">
    <h1>Versionen</h1>
    <ul>
      <?php 
      $arrVersions=$site->versions()->yaml();
//      rsort($arrVersions);
      foreach($arrVersions as $version): ?>
      <li>
        <i class="fas fa-<?= $version['icon']  ?> fa-fw"></i>
        <p class="datum">
          <?= $version['datum'] ?>
        </p>
        <p class="kommentar">
          <?= $version['comment'] ?>
        </p>
      </li>
      <?php endforeach ?>
    </ul>
  </div>
  <div class="content">
    <p>
      <?= $page->text()->kirbytext(); ?>
    </p>
  </div>
</main>


<?php snippet('footer-home') ?>
